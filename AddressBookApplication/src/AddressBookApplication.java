/**
*@author John Lugtu
*@version 1.0
*@since 10/15/15
*/
import java.util.Scanner;
//import java.io.*;
public class AddressBookApplication
{
	static AddressBook AB = new AddressBook();
	static Scanner in = new Scanner(System.in);
	static Menu m = new Menu();
	static int size = 8;
	static String info[] = new String[size];
	static String choice;
	
	public static void main(String[] args) 
	{
		//prompt choices
		do
		{
			m.displayChoices();
			choice = in.nextLine();	
			switch(choice)
			{
			//Load from file
			case "a":
				System.out.println("Enter File Name:");
				AB.readFromFile(in.nextLine());
				break;
			//Add
			case "b":
				prompt();
				AB.addEntry(new AddressEntry(info[0],info[1],info[2],info[3],info[4],info[5],info[6],info[7]));
				System.out.println("Thank you. You have just Entered:\n\n" +
									(new AddressEntry(info[0],info[1],info[2],info[3],info[4],info[5],info[6],info[7])).toString());
				break;
			//Remove
			case "c":
				System.out.println("Enter Last Name of entry to remove:");
				AB.remove(in.nextLine());
				break;
			//Find
			case "d":
				System.out.println("\nPlease Enter last name:");
				if(AB.search(in.nextLine()))
				{	System.out.println("\nFound!\n"); }
				else { System.out.println("\nCould Not Find!\n");}
				
				break;
			//List
			case "e":
				AB.list();
				break;
			//Save and Exit
			case "f":
				AB.storeToFile();
				choice = "f";
				break;
			default:
				System.out.println("Error!! Not a Choice");
			}
		}while(choice != "f");	
	}
	
	/**
	 * Prompts Info
	 * @variable E holds entries after prompted
	 * @return void
	 */
	static private void prompt()
	{
		//array that holds entries
		info[0] = m.prompt_FirstName();
		info[1] = m.prompt_LastName();
		info[2] = m.prompt_Street();
		info[3] = m.prompt_City();
		info[4] = m.prompt_State();
		info[5] = m.prompt_Zip();
		info[6] = m.prompt_Email();
		info[7] = m.prompt_Telephone();
	}
	
}

