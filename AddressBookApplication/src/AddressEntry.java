/**
*@author John Lugtu
*@version 1.0
*@since 10/15/15
*/
/**
 *	purpose: Holds information of the Entry
 */
public class AddressEntry implements Comparable<AddressEntry> {
	private String firstName, lastName, street,
				   city, state, zip, 
				   email, phone;
	String newString;
	
	/**
	 * Constructor to set entries
	 * @param f,l,s,c,st,z,e,p
	 */
	AddressEntry(String f,String l, 
			     String s, String c, String st,
				 String z, String e, String p)
	{
		setFirst(f);
		setLast(l);
		setStreet(s);
		setState(st);
		setCity(c);	
		setZip(z); 
		setEmail(e); 	
		setPhone(p);		
	}
	
	/**
	 * Converts All data collected and change to a nicely formatted String
	 * @convert String
	 */
	public String toString()
	{
		
		newString = firstName;	newString += " ";
		newString += lastName;	newString += "\n";
		newString += street;	newString += "\n";
		newString += city;		newString += ", ";
		newString += state;		newString += " ";
		newString += zip; 		newString += "\n";
		newString += email; 	newString += "\n";
		newString += phone;		newString += "\n";
		return newString;
	}
	
	/**
	 * Converts Info to store into a file
	 * @return String
	 */
	public String toFile()
	{
		
		newString = firstName; newString += "\r\n";
		newString += lastName; newString += "\r\n";
		newString += street;   newString += "\r\n";
		newString += city;	   newString += "\r\n";
		newString += state;	   newString += "\r\n";
		newString += zip;      newString += "\r\n";
		newString += email;    newString += "\r\n";
		newString += phone;    
		return newString;
	}
	
	/**
	 * Setters
	 */
	private void setFirst(String x){ firstName = x;}
	private void setLast(String x){ lastName = x;}
	private void setStreet(String x){ street = x;}
	private void setCity(String x){ city = x;}
	private void setState(String x){ state = x;}
	private void setZip(String x){ zip = x;}
	private void setEmail(String x){ email = x;}
	private void setPhone(String x){ phone = x;}
	
	/**
	 * Getters
	 */
	public String getFirst(){ return firstName;}
	public String getLast(){ return lastName;}
	public String getStreet(){ return street;}
	public String getCity(){ return city;}
	public String getState(){ return state;}
	public String getZip(){ return zip;}
	public String getEmail(){ return email;}
	String getPhone(){ return phone;}
	
	/**
	 * Override compareTo to handle Object AddressEntry to help
	 * in future sort Objects.
	 * @param AddressEntry AE
	 * @return -1,0,1
	 */
	public int compareTo(AddressEntry AE) {
        int lastCmp = lastName.compareTo(AE.getLast());
        return (lastCmp != 0 ? lastCmp : firstName.compareTo(AE.getFirst()));
    }
}
