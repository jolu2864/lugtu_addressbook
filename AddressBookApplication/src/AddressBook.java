/**
*@author John Lugtu
*@version 1.0
*@since 10/15/15
*/
import java.io.*;
import java.util.*;


/**
 * purpose: A Collection to hold Address Entries
 */
public class AddressBook 
{
	private static BufferedReader BR;
	private static TreeSet<AddressEntry> TS = new TreeSet<AddressEntry>();
	private static Scanner in = new Scanner(System.in);
	private static int numEntries = 0;
	
	String s;
	/*
	 * Empty Constructor
	 */
	AddressBook()
	{
		
	}
	/**
	 * Constructor to add Entry
	 * @param AE
	 */
	AddressBook(AddressEntry AE)
	{
		TS.add(AE);
	}
	
	/**
	 * Adds Entry
	 * @param AE
	 */
	static public void addEntry(AddressEntry AE)
	{
		TS.add(AE);
		numEntries++;
	}
	
	/**
	 * Searches and finds last name. 
	 * If found asks (y/n) to remove
	 * If multiple List all entries with last name.
	 * then choose which to remove.
	 * @variables choice: option of deleting the entry you wanted.
	 * 			  entry: the string of Address Entry
	 * @param lastName
	 */
	public void remove(String lastName)
	{
		String choice,
			   entry;
		
		if(search(lastName))
		{
			System.out.println("Please enter the number corresponding to the Entry you want to remove:");
			choice = in.nextLine();
			int i = 1;
			Iterator<AddressEntry> iterator;
			iterator = TS.iterator();
			
			while(iterator.hasNext())
			{
				if(((entry = iterator.next().toString()).toLowerCase()).contains(lastName.toLowerCase()))
				{
					if(Integer.parseInt(choice) == i)
					{
						System.out.println("Are you sure you want to remove the Entry? (y/n)");
						switch(in.next())
						{
						case "y": 
							System.out.println("Successfully removed:\n\n" + entry);
							iterator.remove();
							break;
						default:
							return;
						}
					}
					i++;
				}
			}
			numEntries--;
		}
	}
	
	/**
	 * Prints everything in the list
	 * @variable iterator: used to traverse the tree set
	 * @return void
	 */
	public void list()
	{
		Iterator<AddressEntry> iterator;
		iterator = TS.iterator();
		int i = 1;
		while(iterator.hasNext())
		{
			System.out.println(i + ":\n" + iterator.next().toString()+ "\n");
			i++;
		}
	}
	
	/**
	 * Reads In from file. Uses Buffered Reader to read the file.
	 * @variable info[] Use to store entry's info
	 * @param filename
	 * @return void
	 */
	public void readFromFile(String filename)
	{
		int numFileEntries = 0;
		String info[] = new String[8];
		try 
		{
			BR = new BufferedReader(new FileReader(filename));
			String line = BR.readLine();
			while(line != null)
			{
				for(int i = 0; i < 8; i++)
				{
					info[i] = line;
					line = BR.readLine();
					
				}
				addEntry(new AddressEntry(info[0],info[1],info[2],info[3],info[4],info[5],info[6],info[7]));
				numFileEntries++;
			}
			
			System.out.println(numFileEntries + " Entered from " + filename + "\n Total of " + numEntries + " in Address Book");
			BR.close();
		} catch (FileNotFoundException e1) {
			System.out.println(filename + " not found!");
		} catch(IOException ioe){
			System.out.println("Can't read from file");	
		} 
	}
	
	/**
	 * Stores into File to be saved.
	 * Uses Iterator to traverse the tree set then BufferedWriter to write to file.
	 * @return void
	 */
	public void storeToFile()
	{
		String fileName;
		System.out.println("Enter in the name of the file you wish to save your AddressBook to:");
		fileName = in.nextLine();
		
		BufferedWriter bw = null;
		
		try
		{
			Iterator<AddressEntry> iterator;
			iterator = TS.iterator();
			File file = new File(fileName);
			String buf;
			
			//check if there's a file
			if(!file.exists())
			{
				file.createNewFile();
			}
			
			bw = new BufferedWriter(new FileWriter(file));
			
			while(iterator.hasNext())
			{
				buf = iterator.next().toFile();
				bw.write(buf);
			}
			bw.close();
		} 
		catch(IOException ioe)
		{
			System.out.println("Could not find file!");
			return;
		}
		
	}
	/**
	 * Search and find last name
	 * Converts last name to lower case and address entry to lower case.
	 * Checks whether lastName is in the Address Entry
	 * @param lastName
	 * @return found
	 */
	static public boolean search(String lastName )
	{
		String entry;
		boolean found = false;
		int i = 0;
		
		Iterator<AddressEntry> iterator;
		iterator = TS.iterator();
		
		while(iterator.hasNext())
		{
			if(((entry = iterator.next().toString()).toLowerCase()).contains(lastName.toLowerCase()))
			{
				i++;
				System.out.println(i + ":\n" + entry + "\n");
				found = true;
			}
		}
		System.out.println("There were " + i + " entries with " + lastName);
		
		return found;
	}
	
	

}


