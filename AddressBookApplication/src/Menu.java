/**
*@author John Lugtu
*@version 1.0
*@since 10/15/15
*/
import java.util.Scanner;
/**
 * Prompt User and list a MENU
 */
public class Menu 
{
	static Scanner in = new Scanner(System.in);
	private String temp;
	public Menu()
	{
		
	}
	/**
	 * Display Menu
	 * @return void
	 */
	public void displayChoices()
	{
		System.out.println("*************************************");
		System.out.println("Please enter in your menu selection\n"
							+"a)Loading From File\n\n" 
							+"b)Addition\n\n"
							+"c)Removal\n\n"
							+"d)Find\n\n"
							+"e)Listing\n\n"
							+"f)Save and Quit\n"
							+"*************************************\n");
	}
	/**
	 * Prompt First Name
	 * @return String
	 */
	public String prompt_FirstName()
	{
		System.out.println("First:");
		return setTemp(in.nextLine());
	}
	/**
	 * Prompt First Last
	 * @return String
	 */
	public String prompt_LastName()
	{
		System.out.println("Last:");
		return setTemp(in.nextLine());
	}
	/**
	 * Prompt Street
	 * @return String
	 */
	public String prompt_Street()
	{
		System.out.println("Street:");
		return setTemp(in.nextLine());
	}
	/**
	 * Prompt City
	 * @return String
	 */
	public String prompt_City()
	{
		System.out.println("City:");
		return setTemp(in.nextLine());
	}
	/**
	 * Prompt State
	 * @return String
	 */
	public String prompt_State()
	{
		System.out.println("State:");
		return setTemp(in.nextLine());
	}
	/**
	 * Prompt zip
	 * @return String
	 */
	public String prompt_Zip()
	{
		String x;
		System.out.println("Zip:");
		x = in.nextLine();
		while(!isNumeric(x))
		{
			System.out.println("Error!\n Please Enter Valid Zip:");
			x = in.nextLine();
		}
		return setTemp(x);
	}
	/**
	 * Prompt phone
	 * @return String
	 */
	public String prompt_Telephone()
	{
		String x;
		System.out.println("Telephone:");
		x = in.nextLine();
		while(!isNumeric(x))
		{
			System.out.println("Error!\n Please Enter Valid Phone Number:");
			x = in.nextLine();
		}
		return setTemp(x);
	}
	/**
	 * Prompt email
	 * @return String
	 */
	public String prompt_Email()
	{
		System.out.println("Email:");
		return setTemp(in.nextLine());
	}
	/**
	 * Get's info
	 * @return temp
	 */
	public String getTemp() {
		return temp;
	}
	/**
	 * Set to proper variable
	 * @param temp
	 * @return
	 */
	public String setTemp(String temp) {
		this.temp = temp;
		return temp;
	}
	
	/**
	 * checks if numeric using Regular Expression
	 */
	private boolean isNumeric(String x)
	{
		return x.matches("([0-9]*|-)*");
	}
}
